FROM debian:jessie
MAINTAINER <contato@hacklab.com.br>

ENV DEBIAN_FRONTEND noninteractive
ENV PXC_HOME="/var/cache/mysql"
ENV PXC_DATA="/var/lib/mysql"
ENV PXC_RUN="/var/run/mysqld"

ENV MYSQL_ROOT_PASSWORD="rootpass"
ENV MYSQL_ALLOW_EMPTY_PASSWORD="no"
ENV MYSQL_DATABASE="database"
ENV MYSQL_USER="pxcuser"
ENV MYSQL_PASSWORD="pxcpass"
ENV XTRABACKUP_PASSWORD="password"
ENV MYSQL_LOG_FILE="/var/log/mysqld.log"
ARG version_info="hacklab/docker-pxc"

RUN groupadd -r mysql && useradd -r -m -d "${PXC_HOME}" -g mysql mysql \
    && apt-get update \
    && apt-get install -y curl jq netcat \
    && curl -sLo pxc.deb https://repo.percona.com/apt/percona-release_0.1-5.jessie_all.deb \
    && dpkg -i pxc.deb \
    && apt-get update \
    && apt-get install -y percona-xtradb-cluster-57 \
    && rm -Rf "${PXC_DATA}" "${PXC_RUN}" \
    && mkdir -m 755 -p "${PXC_DATA}" "${PXC_RUN}" \
    && chown -R mysql: "${PXC_DATA}" "${PXC_RUN}" \
    && rm pxc.deb \
    && rm /etc/mysql/my.cnf.old \
    && {                                          \
        date;                                     \
        printf "%-45s %-45s %s" $version_info;    \
    } > ${PXC_HOME}/versioninfo.txt

RUN { \
        echo "[mysqld]"; \
        echo "server-id=1"; \
        echo "datadir=${PXC_DATA}"; \
        echo "log-error=${MYSQL_LOG_FILE}"; \
        echo "socket=${PXC_RUN}/mysqld.sock"; \
        echo "pid-file=${PXC_RUN}/mysqld.pid"; \
        echo "log-bin"; \
        echo "log_slave_updates"; \
        echo "default_storage_engine=InnoDB"; \
        echo "binlog_format=ROW"; \
        echo "innodb_flush_log_at_trx_commit  = 0"; \
        echo "innodb_flush_method             = O_DIRECT"; \
        echo "innodb_file_per_table           = 1"; \
        echo "innodb_autoinc_lock_mode=2"; \
        echo "bind_address = 0.0.0.0"; \
        echo "wsrep_slave_threads=2"; \
        echo "wsrep_cluster_address=gcomm://"; \
        echo "wsrep_provider=/usr/lib/galera3/libgalera_smm.so"; \
        echo "wsrep_cluster_name=Theistareykjarbunga"; \
        echo "wsrep_sst_method=xtrabackup-v2"; \
        echo "wsrep_sst_auth=\"root:\""; \
    } > /etc/mysql/percona-xtradb-cluster.conf.d/mysqld.cnf

VOLUME ["${PXC_DATA}","${PXC_RUN}"]
EXPOSE 3306 4567 4568

COPY entrypoint /entrypoint
ENTRYPOINT ["/bin/bash", "/entrypoint/main.sh"]

CMD ["/usr/sbin/mysqld"]
