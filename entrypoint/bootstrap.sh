#!/bin/bash

bootstrap_sql_root_user () {
    cat <<-EOSQL
        -- What's done in this file shouldn't be replicated
        --  or products like mysql-fabric won't work
        SET @@SESSION.SQL_LOG_BIN=0;
        CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;
        GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
        ALTER USER 'root'@'localhost' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}';
        CREATE USER 'xtrabackup'@'localhost' IDENTIFIED BY '${XTRABACKUP_PASSWORD}';
        GRANT RELOAD,PROCESS,LOCK TABLES,REPLICATION CLIENT ON *.* TO 'xtrabackup'@'localhost';
        GRANT REPLICATION CLIENT ON *.* TO monitor@'%' IDENTIFIED BY 'monitor';
        GRANT PROCESS ON *.* TO monitor@localhost IDENTIFIED BY 'monitor';
        DROP DATABASE IF EXISTS test ;
EOSQL
}

bootstrap_sql_database () {
    printf 'CREATE DATABASE IF NOT EXISTS `%s`;' "$MYSQL_DATABASE"
    echo ''
}

bootstrap_sql_user () {
    printf "CREATE USER '%s'@'%%' IDENTIFIED BY '%s';" "$MYSQL_USER" "$MYSQL_PASSWORD"
    echo ''
}

bootstrap_sql_grant_user () {
    printf "GRANT ALL ON \`%s\`.* TO '%s'@'%%';" "$MYSQL_DATABASE" "$MYSQL_USER"
}

bootstrap_database () {
    sql_file="${PXC_HOME}/bootstrap.sql"

    echo 'USE mysql;' > $sql_file
    bootstrap_sql_root_user >> "$sql_file"

    if [ -n "$MYSQL_DATABASE" ];
    then
        bootstrap_sql_database >> "$sql_file"
    fi

    if [ -n "$MYSQL_USER" ] && [ -n "$MYSQL_PASSWORD" ];
    then
        bootstrap_sql_user >> $sql_file

        if [ -n "$MYSQL_DATABASE" ];
        then
            bootstrap_sql_grant_user >> "$sql_file"
        fi
    fi

    mysqld \
        --defaults-file=/etc/mysql/my.cnf \
        --user=mysql \
        --initialize-insecure \
        --init-file=$sql_file \
        --datadir=$PXC_DATA \
        --basedir=/usr

    rm "$sql_file"
}
