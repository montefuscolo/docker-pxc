#!/bin/bash

get_cluster_addresses () {
    curl -s --header 'Accept: application/json' \
        http://rancher-metadata/2016-07-29/self/service/containers \
        | jq -r '.[].primary_ip'
}
