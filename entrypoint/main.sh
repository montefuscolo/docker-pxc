#!/bin/bash

if [ "$(basename $1)" != "mysqld" ];
then
    exec "$@"
    exit 0;
fi

touch "${MYSQL_LOG_FILE}"
chown mysql: "${MYSQL_LOG_FILE}"
chmod 666 "${MYSQL_LOG_FILE}"
tail -F "${MYSQL_LOG_FILE}" 2>/dev/null &

MY_IP=$(hostname -i | awk '{ print $1 }')
first_time=0

if [ -z "${CLUSTER_NAME}" ];
then
    echo 'Set environment variable CLUSTER_NAME' > /dev/stderr
    exit 1
fi

source /entrypoint/info_server.sh &
source /entrypoint/bootstrap.sh;

if [ -z "$(ls -A $PXC_DATA)" ];
then
    mkdir -m 755 -p "${PXC_DATA}"
    bootstrap_database
    chown -R mysql: "${PXC_DATA}"
    first_time=1
fi

if [ -z "$CLUSTER_JOIN" ];
then
    if [ -n "$ETCD_HOST" ];
    then
        source /entrypoint/discovery_etcd.sh
        register_me_on_queue;

    elif curl -sI http://rancher-metadata > /dev/null;
    then
        source /entrypoint/discovery_rancher.sh
    fi

    CLUSTER_JOIN=$(get_cluster_addresses | sed -Eze 's/^\s*//;s/\s*$//;s/\s+/,/g')
    export CLUSTER_JOIN
fi

IFS=","

online=""
current=""
leader=""
for ip in $CLUSTER_JOIN;
do
    if ping -c2 -i5 -W1 $ip > /dev/null;
    then
        if [ "$ip" != "$MY_IP" ];
        then
            if [ -z "$online" ];
            then
                online="$ip"
            else
                online="$online,$ip"
            fi
        fi

        score=$(curl -s http://$ip:8888/score)
        if [ -z "$current" ] || [[ "$score" -gt "$current" ]];
        then
            current=$score
            leader=$ip
        fi
    fi
done
IFS="
"

echo "#
# My IP: $MY_IP
# Detected IPs: $online
# Leader: $leader
#"

if [ "$leader" = "$MY_IP" ];
then
    online=""
    sed -Eie 's/^(safe_to_bootstrap:).*/\1 1/' "${PXC_DATA}/grastate.dat"

elif [ "$first_time" = "1" ];
then
    tries=30
    while [ "$tries" != "0" ] && ! echo 1 > /dev/tcp/$leader/3306;
    do
        tries=$(( $tries - 1 ))
        echo "Waiting for the leader $leader, I will try more $tries times"
        sleep 5
    done
fi

set -- "$@" "--defaults-file=/etc/mysql/my.cnf"
set -- "$@" "--user=mysql"
set -- "$@" "--datadir=${PXC_DATA}"
set -- "$@" "--log-bin=$CLUSTER_NAME-bin"
set -- "$@" "--explicit_defaults_for_timestamp=on"
set -- "$@" "--wsrep_cluster_name=$CLUSTER_NAME"
set -- "$@" "--wsrep_sst_method=xtrabackup-v2"
set -- "$@" "--wsrep_cluster_address=gcomm://$online"
set -- "$@" "--wsrep_sst_auth=xtrabackup:$XTRABACKUP_PASSWORD"
set -- "$@" "--wsrep_node_address=$MY_IP"
echo "$@" >&2
exec "$@"
